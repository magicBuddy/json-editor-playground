import * as actionTypes from '../actions/actionTypes';

const initialState = {
    jsonStructuredData: {},
    isLoading: false,
    fileUploaded: false,
    optionAsImport: false,
    optionAsUpdate: false,
    convertedAsJSON: false
}

export const jsonDataReducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_JSON_DATA:
            return {
                ...state,
                jsonStructuredData: action.payload
            }
        case actionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case actionTypes.FILE_UPLOADED:
            return {
                ...state,
                fileUploaded: action.payload
            }
        case actionTypes.SET_OPTION_AS_IMPORT:
            return {
                ...state,
                optionAsImport: action.payload
            }
        case actionTypes.SET_OPTION_AS_UPDATE:
            return {
                ...state,
                optionAsUpdate: action.payload
            }
        case actionTypes.CLEAR_ALL_DATA:
            return initialState

        case actionTypes.CONVERTED_AS_JSON:
            return {
                ...state,
                convertedAsJSON: action.payload
            }
        default:
            return state;
    }
}