import { jsonDataReducer } from "./jsonDataReducer";
const { combineReducers } = require("redux");

export const rootReducer = combineReducers({
    json: jsonDataReducer
})