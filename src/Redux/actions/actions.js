import * as actionTypes from './actionTypes'

export const setJsonData = data => {
    return {
        type: actionTypes.SET_JSON_DATA,
        payload: data
    }
}

export const setIsLoading = isLoading => {
    return {
        type: actionTypes.SET_IS_LOADING,
        payload: isLoading
    }
}
