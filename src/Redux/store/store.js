import { rootReducer } from "../reducers/rootReducer";
const { createStore } = require("redux");

export const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())