import { Backdrop, CircularProgress } from '@material-ui/core';
import Axios from 'axios';
import React, { useState, useEffect } from 'react'
import jwt_decode from 'jwt-decode'
import { toast } from 'react-toastify';
import Logo from '../../../Assets/logo.png';

function Login(props) {
    const [email,setEmail] = useState('')
    const [password, setPassword] = useState('')
	const [isBtnDisabled, setIsBtnDisabled] = useState(false)
	const [isLoading, setIsLoading] = useState(false);
	const [errors, setErrors] = useState({});
    useEffect(()=> {
		const sessionToken = localStorage.getItem('SESSION-TOKEN')
        if(sessionToken){
            props.history.push('/')
		}
		
    }, [])

    async function formSubmitHandler(event){
		event.preventDefault();
		if(email.trim().length===0){
			setErrors({email: "Email cannot be empty"})
		}else if(password.trim().length===0){
			setErrors({password:"Password cannot be empty"})
		}else if(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.trim())===false){
			setErrors({email: "Enter a valid email"})
		}else{
			setErrors({});
			setIsLoading(true);
			setIsBtnDisabled(true)
			const loginUser = {email,password}
			let response, loginResponse;
			try {
				response = await Axios.post(process.env.REACT_APP_API_URL+"/generateToken", {}, {
					headers: {
						'ACCESS-KEY': process.env.REACT_APP_API_ACCESS_KEY,
						'ACCESS-SECRET':process.env.REACT_APP_API_ACCESS_SECRET
					}
				})

			}catch(err){
				alert('Oops something went wrong...')
				setIsLoading(false);
				setIsBtnDisabled(false);
				return;
			}
			if(response.data.code===200){
				try {
					loginResponse = await Axios.post(process.env.REACT_APP_API_URL+"/login", loginUser, {
						headers: {
							'APPLICATION-TOKEN': response.data.data.token
						}
					})
				}catch(err){
					console.log(err.response);				
					if(err.response.data.errors && err.response.data.errors.password && err.response.data.errors.password.length>0){
						toast.error('Invalid password!', {
							position: "bottom-left",
							autoClose: 2000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						})
					}else if(err.response.data.errors && err.response.data.errors.email && err.response.data.errors.email.length>0){
						toast.error('Invalid email!', {
							position: "bottom-left",
							autoClose: 2000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						})
					}
					console.log(err);
					setIsLoading(false);
					setIsBtnDisabled(false);
					
					return;
				}
			}
			if(loginResponse.data.code===200 && loginResponse.data.data.steps_completed===5){
				localStorage.setItem("SESSION-TOKEN", loginResponse.data.data.session_token);
				const decoded_token = jwt_decode(loginResponse.data.data.session_token);
				localStorage.setItem("user_id", parseInt(decoded_token.sub));
				setIsLoading(false);
				props.history.push('/');
			}else{
				alert('Oops something went wrong...')
				setIsLoading(false);
				setIsBtnDisabled(false);
				return;
			}
		}
	}
    return (
		<React.Fragment>
			<div className="login">
				<div className="OuterLoginContainer">
					<div className="InnerLoginContainer">
						<div className="logo text-center" style={{marginBottom:"20px"}}>
							<img src={Logo} alt="" style={{width:"50px", height:"auto"}}/>
						</div>
						<h1 className="login_heading text-center">Login</h1>
						<form onSubmit={formSubmitHandler}>

							<div className="mb-4">
								<h5 className="text-white font-weight-light">Email :</h5>
								<input placeholder="Enter Email" className="form-control form-control" required value={email} type="email" onChange={(event)=> setEmail(event.target.value)}/>
								{errors.email ? <span style={{color:"red"}}>{errors.email}</span> : null}
							</div>

							<div className="mb-2">
								<h5 className="text-white font-weight-light">Password :</h5>
								<input placeholder="Enter Password" className="form-control form-control" required value={password} type="password" onChange={(event)=> setPassword(event.target.value)}/>
								{errors.password ? <span style={{color:"red"}}>{errors.password}</span> : null}
							</div>

							<input disabled={isBtnDisabled} className="login-button mt-20" type="submit" value="SIGN IN"/>
						</form>
					</div>
				</div>
			</div>
			<Backdrop style={{color: "#fff", zIndex: 1201}} open={isLoading}>
			<CircularProgress color="inherit" />
			</Backdrop>
		</React.Fragment>
    )
}


export default Login