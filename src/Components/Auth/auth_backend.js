import axios from "axios";

class AuthBackend {
	generateToken() {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/generateToken`,
					{ blank: "_blank" },
					{
						headers: {
							"ACCESS-KEY": process.env.REACT_APP_API_ACCESS_KEY,
							"ACCESS-SECRET": process.env.REACT_APP_API_ACCESS_SECRET,
						},
					}
				)
				.then((response) => {
					if (response.data.code !== 200) throw response.data;
					resolve(response.data.data.token);
				})
				.catch((err) => {
					console.log(err.response.data);
				});
		});
	}

	loginWithEmailAndPassword(token, userEmail, userPassword) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/login`,
					{
						email: userEmail,
						password: userPassword,
					},
					{
						headers: { "APPLICATION-TOKEN": token },
					}
				)
				.then((res) => {
					if (res.data.code !== 200) throw res.data;
					resolve(res.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	createUserWithDetails(
		token,
		// email,
		// password,
		// fullName,
		// nickName,
		// device,
		userPhoto
	) {
		console.log(userPhoto);
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/user`,
					//   {
					//     email: email,
					//     password: password,
					//     full_name: fullName,
					//     nick_name: nickName,
					//     device: device,
					//     profile_icon: userPhoto,
					//   }
					userPhoto,
					{
						headers: {
							"APPLICATION-TOKEN": token,
							"Content-Type": "multipart/form-data",
						},
					}
				)
				.then((response) => {
					if (response.data.code !== 200) throw response.data;
					resolve(response.data.data.session_token);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	verifyEmail(otp, email, token) {
		console.log(otp, email);
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/user/verifyEmail`,
					{
						code: otp,
						email: email,
					},
					{ headers: { "APPLICATION-TOKEN": token } }
				)
				.then((response) => {
					// if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	generateOtp(email, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/user/generateOtp`,
					{
						email: email,
					},
					{ headers: { "APPLICATION-TOKEN": token } }
				)
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	setSocialDetails(formdata, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(`${process.env.REACT_APP_API_URL}/user/socialLogin`, formdata, {
					headers: { "APPLICATION-TOKEN": token },
				})
				.then((response) => {
					console.log(response);
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	setPasswordAndUserName(formdata, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(`${process.env.REACT_APP_API_URL}/user/update`, formdata, {
					headers: { "APPLICATION-TOKEN": token },
				})
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	setFullNameAndNickName(formdata, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(`${process.env.REACT_APP_API_URL}/user/update`, formdata, {
					headers: { "APPLICATION-TOKEN": token },
				})
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	setUserProfilePhoto(formdata, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(`${process.env.REACT_APP_API_URL}/user/photo`, formdata, {
					headers: { "APPLICATION-TOKEN": token },
				})
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	sendForgetPasswordLink(email, token) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/user/forgetPassword`,
					{ email: email },
					{
						headers: { "APPLICATION-TOKEN": token },
					}
				)
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	resetPassword(password, urlToken, applicationToken) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/user/resetPassword`,
					{
						token: urlToken,
						password: password,
					},
					{
						headers: { "APPLICATION-TOKEN": applicationToken },
					}
				)
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}

	getUserInfo(stoken) {
		return new Promise(async (resolve, reject) => {
			await axios
				.post(
					`${process.env.REACT_APP_API_URL}/getProfileInfo`,
					{},
					{
						headers: { "SESSION-TOKEN": stoken },
					}
				)
				.then((response) => {
					if (response.data.code !== 200) throw response;
					resolve(response.data);
				})
				.catch((err) => {
					reject(err.response);
				});
		});
	}
}

export let authBackend = new AuthBackend();
