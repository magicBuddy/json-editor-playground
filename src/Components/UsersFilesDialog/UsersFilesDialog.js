//React

import React from 'react'

//Material UI Components

import { Avatar, Dialog, DialogTitle, List, ListItem, ListItemAvatar, ListItemText } from '@material-ui/core';

import { Add } from '@material-ui/icons';

//UsersFilesDialog Component

function UsersFilesDialog({setOpenAllFilesDialog, openAllFilesDialog, allSavedFiles, openFileAsJsonHandler}) {
    
    return (

        <Dialog onClose={()=>setOpenAllFilesDialog(false)} aria-labelledby="simple-dialog-title" open={openAllFilesDialog}>

          <DialogTitle id="simple-dialog-title" style={{background:"rgb(26,26,26)"}}>

            <span style={{fontSize:"2.5rem", color:"white"}}>Your Saved Files</span>

          </DialogTitle>

          <List style={{background:"#0d0d0f"}}>

          {

            allSavedFiles.length===0 ? <h6 style={{color:"white", fontSize:"2.5rem", textAlign:"center"}}>No saved files yet.</h6> :

            allSavedFiles.map((file,index) => 

            <ListItem key={index} autoFocus button onClick={()=>openFileAsJsonHandler(file.url,file.id)}>

              <ListItemAvatar>                

                  <Avatar data-toggle="tooltip" title="Open File">

                    <Add style={{color:"black", fontWeight:500}} />

                  </Avatar>

              </ListItemAvatar>

              <ListItemText>

                <span style={{color:"#fdd344", fontSize:"2rem"}}>{file.url.split("/")[file.url.split("/").length-1].split(".")[0]}</span>

              </ListItemText>

            </ListItem>

          )}

          </List>

      </Dialog>

    )

}

export default UsersFilesDialog
