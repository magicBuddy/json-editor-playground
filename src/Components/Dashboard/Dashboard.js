//React

import React, { useEffect, useState } from 'react';

import JSONSchemaForm from '../JSONSchemaForm/JSONSchemaForm';

import ReactJson from "react-json-view";

import exportFromJSON from 'export-from-json'

//Material UI Components

import GetAppIcon from '@material-ui/icons/GetApp';

import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import { Backdrop, Button, CircularProgress, Tooltip } from '@material-ui/core';

import { Create, KeyboardBackspaceOutlined, Save } from '@material-ui/icons';

import DescriptionIcon from '@material-ui/icons/Description';

import ExitToApp from '@material-ui/icons/ExitToAppOutlined';

import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

//Asset

import HiverlabLogo from '../../Assets/hiverlab-logo.png';

//Axios Instance

import Axios from 'axios';

//Redux components

import { connect } from 'react-redux';

import { setJsonData } from '../../Redux/actions/actions';

import { store } from '../../Redux/store/store';

import { CLEAR_ALL_DATA, FILE_UPLOADED, SET_OPTION_AS_IMPORT, SET_OPTION_AS_UPDATE } from '../../Redux/actions/actionTypes';

//Toast components

import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import UsersFilesDialog from '../UsersFilesDialog/UsersFilesDialog';

toast.configure();

//Dashboard Component

function Dashboard({setJSONFileData, JSONFileData, history}) {

  const [res, setRes] = useState({});

  const [openAllFilesDialog, setOpenAllFilesDialog] = useState(false)

  const [allSavedFiles, setAllSavedFiles] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  const [isJsonStructureVisible, setIsJsonStructureVisible] = useState(true);

  const [optionAsUpdate, setOptionAsUpdate] = useState(false);

  const [currentFileId, setCurrentFileId] = useState("");

  const [currentFileName, setCurrentFileName] = useState("");
    
  //If token does not exist, redirect to login page

  useEffect(()=> {

    if(!(!!localStorage.getItem("SESSION-TOKEN"))) {

      history.push('/login')

    }

  }, [])
  
  //If JSON file imported use this file to set the form and json structure

  useEffect(()=> {

    if(JSONFileData && Object.keys(JSONFileData).length>0) {

      setRes(JSONFileData)

    }

  }, [res, JSONFileData])

  
  //Calling handleFileRead() when as soon as file is imported and can be read as text file

  let fileReader;

  const handleFileRead = (e) => {
    
    const content = fileReader.result;

    const parsedContent = JSON.parse(content);

    setJSONFileData(parsedContent);
  
  }

  //Calling fileChangeHandler as a JSON file is imported

  const fileChangeHandler = event => {

    setOptionAsUpdate(false);

    store.dispatch({

      type: SET_OPTION_AS_IMPORT,
      payload: true

    })

    const file = event.target.files[0];

    if (file.type !== "application/json") {
      
      alert('Please upload a JSON file.')

      return;

    } else {
      
      fileReader = new FileReader();

      fileReader.onloadend = handleFileRead;
      
      fileReader.readAsText(file);

      store.dispatch({

        type: FILE_UPLOADED,
        payload: true

      })

    }

  }

  //Calling openFileAsJsonHandler() as soon as a JSON file is selected from user's All Files section

  const openFileAsJsonHandler = (file,id) => {

    store.dispatch({

      type: SET_OPTION_AS_UPDATE,
      payload: true

    })

    setOptionAsUpdate(true); // showing user the option of "update file" instead "save"

    setCurrentFileId(id); //setting file ID of selected file

    setCurrentFileName(file.split("/")[file.split("/").length - 1].split(".")[0]); // setting file name of selected file

    setIsLoading(true);

    setOpenAllFilesDialog(false); // closing the All Files popup

    //fetching the hosted file from the file-url

    var url = 'https://cors-anywhere.herokuapp.com/' +file;
    
    fetch(url)

      .then(function (response) {
    
        response.text()

          .then(function (text) {
  
            const jsonData = JSON.parse(text); // after fetching the file
  
            setJSONFileData(jsonData);
  
            setIsLoading(false);
    
        });
    
      })
    
      .catch(err=> {
    
        console.log(err);

        setIsLoading(false);

        alert('Something went wrong...');

        return
    
      })
  }
  
  //calling fetchAllSavedFilesHandler() for fetching all user's files through API

  async function fetchAllSavedFilesHandler() {

    setIsLoading(true);

    let response;

    try {

      response = await Axios.get(`${process.env.REACT_APP_API_URL}/json-editor/getExistingJSONs`, {

        headers: {
            'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN") 
          }

      })

    } catch(err) {

      console.log(err)
    
      setIsLoading(false);
    
      alert('Something went wrong!!!')

      return

    }
    
    if(response) {
    
      setIsLoading(false);
    
      setAllSavedFiles(response.data.data); // setting all saved files in component state
    
      setOpenAllFilesDialog(true);
    
    }

  }
  
  //Calling udatedJsonFileHandler() to update the current selected file and saving it in db

  async function updateJsonFileHandler() {
  
    if(!currentFileName) {
  
      alert("Something wrong with the choosen file, try importing file again")

      return;
  
    }
  
    setIsLoading(true);
  
    const jsonFile = new File([JSON.stringify(res, null, 4)], currentFileName, { type: 'application/json' }); //creating File Object of the updated JSON structure in order to send to backend

      const formData = new FormData();

      formData.append('file', jsonFile);
      
      formData.append('user_id', localStorage.getItem("user_id"));
      
      formData.append('json_editor_id', currentFileId);
      
      formData.append('file_name', currentFileName);
      
      let response;
      
      try {
      
      response = await Axios.post(`${process.env.REACT_APP_API_URL}/json-editor/uploadEditorJSON`, formData, {

          headers: {
            'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
          }
          
        })
      
    } catch (err) {

        console.log(err);

        setIsLoading(false)

        alert('Oops..something went wrong!')
        
        return;

      }

      if (response) {

        setIsLoading(false);

        toast.info('JSON file updated successfully!', {

          position: "bottom-left",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true

        }) // showing toast on successfully updating file to db

      }

  }

  //calling saveJsonFileHandler() for saving the json by converting it into a file object and sending to backend

  async function saveJsonFileHandler() {

    setIsLoading(true);

    const jsonFile = new File([JSON.stringify(res, null, 4)], new Date().getTime().toString(),{ type: 'application/json' }); //creating File Object of the current JSON structure in order to send to backend

    const formData = new FormData();

    formData.append('file', jsonFile)

    formData.append('user_id', localStorage.getItem("user_id"))

    let response;

    try {

      response = await Axios.post(`${process.env.REACT_APP_API_URL}/json-editor/uploadEditorJSON`, formData, {

        headers: {
          'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
        }

      })

    } catch(err) {

      console.log(err);

      setIsLoading(false)

      alert('Oops..something went wrong!')

      return;

    }

    if(response) {

      setIsLoading(false);

      toast.success('JSON file saved successfully!', {

        position: "bottom-left",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true

      }) // showing toast on successfully saving file to db 

    }

  }

  //calling downJsonFileHandler() to download the current JSON structure as a file on user's local machine

  const downloadJsonFileHandler = () => {

    const data = JSON.stringify(res, null, 4)

    const fileName = new Date().getTime().toString();

    const exportType = 'json'

    exportFromJSON({ data, fileName, exportType }) // allows user to download JSON file on local machine

  }
  
  //calling logoutUserHandler() to logout user from the app

  async function logoutUserHandler() {

    setIsLoading(true);

    let response;

    try {

      response = await Axios.post(process.env.REACT_APP_API_URL+"/logout", {}, {

        headers: {
          "SESSION-TOKEN": localStorage.getItem("SESSION-TOKEN")
        }

      })

    } catch(err) {

      console.log(err)

      setIsLoading(false);

      if(err.response.code===401) {

        localStorage.removeItem("SESSION-TOKEN")
        
        history.push('/login')

        return;

      }

      alert('Something went wrong..')

      return;

    }

    //if logout response successful, clear all current data from state and redux

    if(response) {

      setIsLoading(false);

      localStorage.removeItem("SESSION-TOKEN")

      localStorage.removeItem("user_id")

      setCurrentFileId("");

      setCurrentFileName("");
      
      store.dispatch({

        type: CLEAR_ALL_DATA

      })

      history.push('/login')

      return;

    }

  }

  //calling jsonStructureVisibilityHandler() if user wants to hide/collapse the JSON Structure section from the screen 

  const jsonStructureVisibilityHander = () => {

    setIsJsonStructureVisible(isJsonStructureVisible=> !isJsonStructureVisible);

  }

  return (

    <React.Fragment>
      
      {/* Header section */}

      <div className="top-bar" style={{background:"#1A1A1A", color:"white"}}>

        <div className="row" style={{margin:"auto 1%"}}>

          {/* Hiverlab logo */}

          <div className="col-sm-4" style={{padding:"0 7px"}}>

            <div className="hiverlab-logo" style={{padding:"10px 0"}}>

              <img src={HiverlabLogo} alt="" style={{width:"105px", maxWidth:"100%", height:"auto", color:"#FDD344"}}/>

            </div>
          
          </div>
        
          {/* Title for Editor */}

          <div className="col-sm-4">
          
          <h1 className="editor-title" style={{marginTop:"16px", fontSize:"1.7em"}}>CloudExpo JSON Editor</h1>
          
          </div>
        
          {/* Actions for upload/download/save/update/logout  */}

          <div className="col-sm-4">
          
            <div className="options-title" style={{marginTop:"11px"}}>
          
                {/* All Files Option  */}

                <Button className="icon-button" component="span" onClick={fetchAllSavedFilesHandler}><DescriptionIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"4px", color:"#FDD344"}}>All Your Files</span></Button>
          
                {/* Upload File Locally Option */}
                
                <input type="file" accept="application/json" onChange={(e)=>fileChangeHandler(e)} style={{display: "none"}} id="upload-file" />
          
                <label htmlFor="upload-file">            
                  
                    <Button className="icon-button" component="span" aria-label="Upload File"><CloudUploadIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"5px", color:"#FDD344"}}>Upload</span></Button>
                  
                </label>
          
                {/* Download JSON File Option  */}

                { Object.keys(res).length>0 ?
            
                    <Button className="icon-button" component="span" onClick={downloadJsonFileHandler}><GetAppIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"1px", color:"#FDD344"}}>Download</span></Button>
            
                : null }

                {/* Saving JSON File Option  */}
          
                { Object.keys(res).length>0 ?  
            
                    !optionAsUpdate ?                 
            
                    <Button className="icon-button" component="span" onClick={saveJsonFileHandler}><Save style={{color:"#FDD344", fontSize:"1.3em", marginTop:"-3px"}} className="text-white"/><span style={{marginLeft:"3px", color:"#FDD344"}}>Save</span></Button>
            
                    : 
            
                    <Button className="icon-button" component="span" onClick={updateJsonFileHandler}><Create style={{color:"#FDD344", fontSize:"1.3em", marginTop:"-1px"}} className="text-white"/><span style={{marginLeft:"3px", color:"#FDD344"}}>Update File</span></Button>
            
                : null }

                {/* Logout user option  */}
          
                <Button className="icon-button" component="span" onClick={logoutUserHandler}><ExitToApp style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"4px", color:"#FDD344"}}>Logout</span></Button>
          
            </div>
          
          </div>
        
        </div>
      
      </div>

      {/* End of Header Section */}

      {/* Main section  */}
        
      <div className="m-2 mt-3 p-4 bg-light">
  
        <div className="row" style={{marginRight:"0px"}}>
  
          {/* JSON Form Editor section  */}

          <div className={`${isJsonStructureVisible ? "col-lg-6" : "col-lg-11"} col-md-12 col-sm-12 mb-5 editor-form`}>
  
            <h2 style={{color:"#FDD344", fontSize:"18px"}}>EDITOR</h2>
  
            <JSONSchemaForm handleSubmit={payload => setRes(payload)} />
  
          </div>

          {/* End of JSON Form Editor section  */}
                  
          {/* JSON Structure section  */}

          {isJsonStructureVisible ? 
  
          <div className="col-lg-6 col-md-12 col-sm-12 mb-5 json-structure">
  
            <div className="heading" style={{display:"flex", justifyContent:"flex-start", alignItems:"center"}}>
  
              <h2 style={{color:"#FDD344", fontSize:"18px"}}>JSON STRUCTURE</h2>
  
              <Tooltip arrow placement="right" title={<span style={{fontSize:"1rem", padding:"10px 5px"}}>Hide JSON Structure</span>}>
  
                <ArrowRightAltIcon onClick={jsonStructureVisibilityHander} style={{cursor:"pointer", alignSelf:"center", marginTop:"11px", marginLeft:"10px", fontSize:"3.5rem"}}/>
  
              </Tooltip>
  
            </div>
  
            <ReactJson src={res} />
  
          </div>
                        
          : 
          
          <div className="col-lg-1">
          
            <Tooltip placement="bottom" arrow title={<span style={{fontSize:"1rem", padding:"10px 5px"}}>Show JSON Structure</span>}>
          
              <KeyboardBackspaceOutlined onClick={jsonStructureVisibilityHander} style={{cursor:"pointer", marginTop:"11px", marginLeft:"10px", fontSize:"3.5rem"}}/>
          
            </Tooltip>
          
          </div>            
          
          }

          {/* End Of Json Structure section  */}
        
        </div>
      
      </div>

      {/* End Of Main Section  */}

      {/* Users All Files Dialog  */}

        <UsersFilesDialog 

          setOpenAllFilesDialog={setOpenAllFilesDialog} 

          openAllFilesDialog={openAllFilesDialog}
          
          allSavedFiles={allSavedFiles}

          openFileAsJsonHandler={openFileAsJsonHandler}

        />

      {/* End Of Users All Files Dialog  */}

      {/* Backdrop section  */}

      <Backdrop style={{color: "#fff", zIndex: 1201}} open={isLoading}>

        <CircularProgress color="inherit" />

      </Backdrop>

      {/* End Of Backdrop section  */}

    </React.Fragment>

  );

}

const mapStateToProps = state => {

  return {

    JSONFileData: state.json.jsonStructuredData

  }

}

const mapDispatchToProps = dispatchEvent => {

  return {

    setJSONFileData: (data) => dispatchEvent(setJsonData(data))

  }

}

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard);
