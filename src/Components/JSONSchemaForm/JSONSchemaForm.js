// React

import React from "react";

// JSON Form 

import Form from "@rjsf/core";

// JSON Schema

import { schema } from "../../Config/Schema/schema";

// Material UI Components 

import { Backdrop, CircularProgress, IconButton } from "@material-ui/core";

import { PhotoCamera } from "@material-ui/icons";

// Redux components 

import { connect } from "react-redux";

import { store } from "../../Redux/store/store";

import { SET_IS_LOADING, SET_JSON_DATA } from "../../Redux/actions/actionTypes";

// Axios instance

import Axios from "axios";

// Temp arrays for saving content-url & thumbnail-url for media, thumbnail-url for interactives and background-url for scenes

let contentUrlMedia = [];

let thumbnailUrlInteractives = [];

let backgroundUrlForScenes = [];

let thumbnailUrlMedia = [];

// JSONSchemaForm Component 

class JSONSchemaForm extends React.Component {

  //Calling handleSubmit for converting form to JSON structure

  async handleSubmit(e) {

    store.dispatch({

      type: SET_IS_LOADING,
      payload: true

    })

    let tempFormData = e.formData;

    if(tempFormData.scenes) {
      
      for(let i=0; i<tempFormData.scenes.length; i++) {

        if(tempFormData.scenes[i]["background-url"]  && !tempFormData.scenes[i]["background-url"].startsWith("http")) {

          let fileType=""

          if ((
            
            tempFormData.scenes[i]["background-url"].slice(-3).toLowerCase() === "jpg" ||
            
            tempFormData.scenes[i]["background-url"].slice(-3).toLowerCase() === "png" ||
            
            tempFormData.scenes[i]["background-url"].slice(-4).toLowerCase() === "jpeg"
          
          )) {

            fileType = "image"

          }
          
          let imageUrl = backgroundUrlForScenes.filter(file => file.name === tempFormData.scenes[i]["background-url"])[0]
          
          const form_Data = new FormData();
          
          form_Data.append('file', imageUrl);
          
          let response;
          
          try {
          
            response = await Axios.post(`${process.env.REACT_APP_API_URL}/uploadAsset?file_type=${fileType}`, form_Data, {
          
              headers: {
                  'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
          
              }
          
            })
            
          } catch (err) {
          
            if (err) {
          
              alert('Something went wrong...')
          
              return;
          
            }
          
          }
          
          if (response) {
          
            tempFormData.scenes[i]["background-url"] = response.data.data.oss_ref;
          
          }
        
        }
        
        if(tempFormData.scenes[i].interactives && tempFormData.scenes[i].interactives.length > 0) {
    
          for(let j=0; j<tempFormData.scenes[i].interactives.length; j++) {
    
            let fileType="";
    
            if (tempFormData.scenes[i].interactives[j]["thumbnail-url"] && !tempFormData.scenes[i].interactives[j]["thumbnail-url"].startsWith("http")) {
    
              if ((
                
                tempFormData.scenes[i].interactives[j]["thumbnail-url"].slice(-3).toLowerCase() === "jpg" ||
    
                tempFormData.scenes[i].interactives[j]["thumbnail-url"].slice(-3).toLowerCase() === "png" ||
    
                tempFormData.scenes[i].interactives[j]["thumbnail-url"].slice(-4).toLowerCase() === "jpeg"
              
              )) {
    
                fileType = "image"

              }
              
              let imageUrl = thumbnailUrlInteractives.filter(file => file.name===tempFormData.scenes[i].interactives[j]["thumbnail-url"])[0]
              
              const form_Data = new FormData();
              
              form_Data.append('file',imageUrl);
              
              let response;
              
              try {
              
                response = await Axios.post(`${process.env.REACT_APP_API_URL}/uploadAsset?file_type=${fileType}`, form_Data, {
              
                  headers: {
                    'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
                  }
              
                })
              
              } catch(err) {
            
                if(err) {
            
                  alert('Something went wrong...')
            
                  return;
            
                }
            
              }
            
              if(response) {
            
                tempFormData.scenes[i].interactives[j]["thumbnail-url"] = response.data.data.oss_ref;
            
              }
            
            }
          
          }
        
        }
        
        if(tempFormData.scenes[i].media && tempFormData.scenes[i].media.length > 0) {
        
          for(let j=0; j<tempFormData.scenes[i].media.length; j++) {
                      
            let fileType="";
        
            if(tempFormData.scenes[i].media[j]["content-url"] && !tempFormData.scenes[i].media[j]["content-url"].startsWith("http")) {
        
              if ((
                
                tempFormData.scenes[i].media[j]["content-url"].slice(-3).toLowerCase() === "jpg" ||
        
                tempFormData.scenes[i].media[j]["content-url"].slice(-3).toLowerCase() === "png" ||
        
                tempFormData.scenes[i].media[j]["content-url"].slice(-4).toLowerCase() === "jpeg"
              
              )) {
              
                fileType="image"
              
              }
              
              else if (
                
                tempFormData.scenes[i].media[j]["content-url"].slice(-3).toLowerCase() === "mp4" ||
                
                tempFormData.scenes[i].media[j]["content-url"].slice(-3).toLowerCase() === "mov"
              
              ) {
            
                  fileType="video"
              
              }
              
              else {

                break;

              }
              
              let imageUrl = contentUrlMedia.filter(file => file.name===tempFormData.scenes[i].media[j]["content-url"])[0]
              
              const form_Data = new FormData();
              
              form_Data.append('file', imageUrl);
              
              let response;
              
              try {
              
                response = await Axios.post(`${process.env.REACT_APP_API_URL}/uploadAsset?file_type=${fileType}`, form_Data, {
              
                  headers: {
                      'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
                    }
              
                  })
              
              } catch (err) {
                  
                if(err){
                
                  alert('Something went wrong...')
                
                  return;
                
                }
              
              }
              
              if(response) {

                tempFormData.scenes[i].media[j]["content-url"] = response.data.data.oss_ref;
                
              }
            
            }

            if (tempFormData.scenes[i].media[j]["thumbnail-url"] && !tempFormData.scenes[i].media[j]["thumbnail-url"].startsWith("http")) {

              if ((

                  tempFormData.scenes[i].media[j]["thumbnail-url"].slice(-3).toLowerCase() === "jpg" ||

                  tempFormData.scenes[i].media[j]["thumbnail-url"].slice(-3).toLowerCase() === "png" ||

                  tempFormData.scenes[i].media[j]["thumbnail-url"].slice(-4).toLowerCase() === "jpeg"

                )) {

                fileType = "image"

              } else {

                break;

              }

              let imageUrl = thumbnailUrlMedia.filter(file => file.name === tempFormData.scenes[i].media[j]["thumbnail-url"])[0]

              const form_Data = new FormData();

              form_Data.append('file', imageUrl);

              let response;

              try {

                response = await Axios.post(`${process.env.REACT_APP_API_URL}/uploadAsset?file_type=${fileType}`, form_Data, {

                  headers: {
                    'SESSION-TOKEN': localStorage.getItem("SESSION-TOKEN")
                  }

                })

              } catch (err) {

                if (err) {

                  alert('Something went wrong...')

                  return;

                }

              }

              if (response) {

                tempFormData.scenes[i].media[j]["thumbnail-url"] = response.data.data.oss_ref;

              }

            }

          }

        }

      }
      
    }

    store.dispatch({

      type: SET_IS_LOADING,
      payload: false

    })

    store.dispatch({

      type: SET_JSON_DATA,
      payload: tempFormData

    }) 

  }

  render() {
  
    let prefilledData;
  
    if(this.props.JSONFileData) {
  
      prefilledData = this.props.JSONFileData
  
    }
    
    let uiSchema = {
  
      "scenes": {
  
        "items":{
  
          'background-url': {
  
            "ui:widget": (props) => {
  
              return (
  
                <React.Fragment>
                
                  <input 
                  
                    type="file"
                  
                    style={{display:"none"}}
                  
                    id={props.id}
                  
                    accept=".jpg, .png"
                  
                    required={props.required}
                  
                    onChange={(event)=> {
                   
                      if (event.target.files[0].type !== "image/png" && event.target.files[0].type !== "image/jpg" && event.target.files[0].type !== "image/jpeg") {
                        
                        alert('Only .jpg, .png files are acceptable')
                        
                        return;
                      
                      }
                      
                      if(event.target.files[0].type.startsWith("image")) {
                      
                        const reader = new FileReader();
                        
                        reader.onload = function (e) {
                        
                          if (document.getElementById(`background-img-${props.id}`).src) {
                        
                            document.getElementById(`background-img-${props.id}`).removeAttribute("src");
                        
                            document.getElementById(`background-img-${props.id}`).src = e.target.result;
                        
                          } 
                          
                          else {
                        
                            document.getElementById(`background-img-${props.id}`).src = e.target.result;
                        
                          }
                        
                          document.getElementById(`background-img-${props.id}`).style.display = "block";
                        
                        }
                        
                        reader.readAsDataURL(event.target.files[0])
                        
                      }
                        
                      props.onChange(event.target.files[0].name)
                      
                      backgroundUrlForScenes.push(event.target.files[0])
                    
                    }} 

                  />

                  <label htmlFor={props.id} style={{display:"block"}}>
                  
                    <IconButton aria-label="upload picture" component="span">
                
                      <PhotoCamera style={{color: "#fdd344", fontSize:"2.5rem"}}/>
                
                    </IconButton>
                  
                  </label>
                  
                  <img alt=" " id={`background-img-${props.id}`} src={props.value} style={{display:`${props.value ? "block" : "none"}`, width:"90px", marginTop:"10px", height:"auto"}}/>
                
                </React.Fragment>

              )

            }

          },

          "interactives": {
          
            "items":{
          
              "thumbnail-url":{
          
                "ui:widget": (props) => {
          
                  return (
          
                    <React.Fragment>
                    
                      <label htmlFor={props.id} style={{display:"block"}}>
                    
                        <IconButton aria-label="upload picture" component="span">
                  
                          <PhotoCamera style={{color: "#fdd344", fontSize:"2.5rem"}}  />
                  
                        </IconButton>
                    
                      </label>
                    
                      <input 
                        
                        type="file"
                        
                        style={{display:"none"}}
                        
                        id={props.id}
                        
                        accept=".jpg, .png"
                        
                        required={props.required}
                        
                        onChange={(event) => {
                        
                          if(event.target.files[0].type !== "image/png" && event.target.files[0].type !== "image/jpg" && event.target.files[0].type !== "image/jpeg") {
                        
                            alert('Only .jpg, .png files are acceptable')
                        
                            return;
                        
                          }

                          if(event.target.files[0].type.startsWith("image"))  {
                            
                            const reader = new FileReader();
                            
                            reader.onload = function (e) {
                            
                            if (document.getElementById(`thumbnail-img-${props.id}`).src) {
                            
                              document.getElementById(`thumbnail-img-${props.id}`).removeAttribute("src");
                            
                              document.getElementById(`thumbnail-img-${props.id}`).src = e.target.result;
                            
                            } 
                            
                            else {
                            
                              document.getElementById(`thumbnail-img-${props.id}`).src = e.target.result;
                            
                            }
                            
                            document.getElementById(`thumbnail-img-${props.id}`).style.display = "block";
                            
                            }
                            
                            reader.readAsDataURL(event.target.files[0])
                          
                          }
                          
                          props.onChange(event.target.files[0].name)
                          
                          thumbnailUrlInteractives.push(event.target.files[0])
                        
                        }} 

                      />
                      
                      <img alt=" " id={`thumbnail-img-${props.id}`} src={props.value} style={{display :`${props.value ? "block" : "none"}`, width:"90px", marginTop:"10px", height:"auto"}}/>
                    
                    </React.Fragment>
                  
                  );
                
                }
              
              }
            
            }
          
          },
          
          "media": {
            
            "items": {
            
              "content-url": {
            
                "ui:widget": (props) => {
            
                  return (
            
                    <React.Fragment> 
                    
                      <label htmlFor={props.id} style={{display:"block"}}>
                    
                        <IconButton aria-label="upload picture" component="span">
                    
                          <PhotoCamera style={{color: "#fdd344", fontSize:"2.5rem"}}  />
                    
                        </IconButton>
                    
                      </label>
                                         
                      <input 
                        
                        type="file"
                        
                        style={{display:"none"}}
                        
                        id={props.id}
                        
                        accept=".jpg, .png, .mp4, .mov"
                        
                        required={props.required}                      
                        
                        onChange={(event) => {
                        
                          if (
                            
                            (event.target.files[0].type !== "image/png") && 
                            
                            (event.target.files[0].type !== "image/jpg") && 
                            
                            (event.target.files[0].type !== "image/jpeg") && 
                            
                            (event.target.files[0].type !== "video/mp4") && 
                            
                            (event.target.files[0].type !== "video/mov")
                            
                          )
                            
                          {
                        
                            alert('Only .jpg, .png, .mp4, .mov files are acceptable')
                        
                            return;
                        
                          }
                          
                          if(event.target.files[0].type.startsWith("image")) {
                          
                            const reader = new FileReader();
                          
                            reader.onload = function(e) {
                          
                              if (document.getElementById(`content-img-${props.id}`).src) {
                          
                                document.getElementById(`content-img-${props.id}`).removeAttribute("src");
                          
                                document.getElementById(`content-img-${props.id}`).src = e.target.result;
                          
                              }
                              
                              else {
                              
                                document.getElementById(`content-img-${props.id}`).src = e.target.result;
                              
                              }
                              
                              document.getElementById(`content-img-${props.id}`).style.display = "block";
                              
                              document.getElementById(`content-video-${props.id}`).style.display = "none";
                            
                            }

                            reader.readAsDataURL(event.target.files[0])

                          } 
                          
                          else if(event.target.files[0].type.startsWith("video")) {
                            
                            const reader = new FileReader();
                            
                            reader.onload = function (e) {
                            
                              if (document.getElementById(`content-video-${props.id}`).src) {
                            
                                document.getElementById(`content-video-${props.id}`).removeAttribute("src");
                            
                                document.getElementById(`content-video-${props.id}`).src = e.target.result;
                            
                              } 
                              
                              else {

                                document.getElementById(`content-video-${props.id}`).src = e.target.result;
                              
                              }
                              
                              document.getElementById(`content-video-${props.id}`).style.display = "block";
                              
                              document.getElementById(`content-img-${props.id}`).style.display = "none";
                            
                            }
                            
                            reader.readAsDataURL(event.target.files[0])
                          
                          }
                          
                          contentUrlMedia.push(event.target.files[0])
                        
                          props.onChange(event.target.files[0].name);

                          }} 

                        />
                       
                       <img alt=" " id={`content-img-${props.id}`} src={props.value} style={{display:`${props.value ? "block" : "none"}`,width:"90px", marginTop:"10px", height:"auto"}}/>
                       
                       <video alt=" " id={`content-video-${props.id}`} src={props.value} style={{display:`${props.value ? "block" : "none"}`,width:"90px", marginTop:"10px", height:"auto"}}/> 
                      
                    </React.Fragment>

                  );

                }

              },
              "thumbnail-url": {

                "ui:widget": (props) => {
            
                  return (
            
                    <React.Fragment> 
                    
                      <label htmlFor={props.id} style={{display:"block"}}>
                    
                        <IconButton aria-label="upload picture" component="span">
                    
                          <PhotoCamera style={{color: "#fdd344", fontSize:"2.5rem"}}  />
                    
                        </IconButton>
                    
                      </label>
                                         
                      <input 
                        
                        type="file"
                        
                        style={{display:"none"}}
                        
                        id={props.id}
                        
                        accept=".jpg, .png, .jpeg"
                        
                        required={props.required}                      
                        
                        onChange={(event) => {
                        
                          if (
                            
                            (event.target.files[0].type !== "image/png") && 
                            
                            (event.target.files[0].type !== "image/jpg") && 
                            
                            (event.target.files[0].type !== "image/jpeg") 
                            
                          )
                            
                          {
                        
                            alert('Only .jpg/.jpeg or .png files are acceptable')
                        
                            return;
                        
                          }
                          
                          if(event.target.files[0].type.startsWith("image")) {
                          
                            const reader = new FileReader();
                          
                            reader.onload = function(e) {
                          
                              if (document.getElementById(`thumbnail-media-img-${props.id}`).src) {
                          
                                document.getElementById(`thumbnail-media-img-${props.id}`).removeAttribute("src");
                          
                                document.getElementById(`thumbnail-media-img-${props.id}`).src = e.target.result;
                          
                              }
                              
                              else {
                              
                                document.getElementById(`thumbnail-media-img-${props.id}`).src = e.target.result;
                              
                              }
                              
                              document.getElementById(`thumbnail-media-img-${props.id}`).style.display = "block";
                              
                            }

                            reader.readAsDataURL(event.target.files[0])

                          } 
                                                    
                          thumbnailUrlMedia.push(event.target.files[0])
                        
                          props.onChange(event.target.files[0].name);

                          }} 

                        />
                       
                       <img alt=" " id={`thumbnail-media-img-${props.id}`} src={props.value} style={{display:`${props.value ? "block" : "none"}`,width:"90px", marginTop:"10px", height:"auto"}}/>
                      
                    </React.Fragment>

                  );

                }

              }

            }

          }

        }

      }

    };

    function transformErrors(errors) {

      const customErrors = errors.map(error=> {

        if(error.name==="enum" || error.name==="oneOf") {

          error.message = "Please select a specific Type."

        }

        return error

      })

      return customErrors.splice(0,1)

    }

    return (

      <React.Fragment>
      
        <Form 

          schema={schema} 
          
          liveValidate 
          
          noHtml5Validate 
          
          uiSchema={uiSchema} 
          
          transformErrors={transformErrors} 
          
          formData={prefilledData} 
          
          onSubmit={(e) => this.handleSubmit(e)}
        >
      
          <button className="btn btn-primary" style={{backgroundColor:"#FDD344", borderColor:"#FDD344", color:"black"}} type="submit">Convert To JSON</button>
      
        </Form>
      
        <Backdrop style={{color: "#fff", zIndex: 1201}} open={this.props.isLoading}>
      
          <CircularProgress color="inherit" />
      
        </Backdrop>
      
      </React.Fragment>
    
    )
  
  }

}

const mapStateToProps = state => {

  return {

    JSONFileData: state.json.jsonStructuredData,

    isLoading: state.json.isLoading,

    fileUploaded: state.json.fileUploaded,

    optionAsImport: state.json.optionAsImport,

    optionAsUpdate: state.json.optionAsUpdate,

    convertedAsJson: state.json.convertedAsJSON

  }

}

export default connect(mapStateToProps, null)(JSONSchemaForm);
