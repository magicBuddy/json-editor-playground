export const schema = {
  title: "Project",
  type: "object",
  properties: {
      id: {
        type: 'string',
        title: 'Project Id',
      },
      scenes: {
        type: 'array',
        title: 'Scenes',
        // 'uniqueItems': true,
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'string',
            },
            'display-name': {
                type: 'string'
            },
            'rotation': {
                type: 'string',
            },
            'background-url': {
                type: 'string'
            },
            interactives: {
              type: 'array',
              title: 'Interactives',
              items: {
                  type: 'object',
                  title: 'Interactive',
                  properties: {
                      id: {
                          type: 'string',
                          default:''
                      },
                      type: {
                          type: 'string',
                          default: '',
                          enum: ['Select Type','Navigation', 'Image', 'Video', 'Embedded', 'Pdf','Custom']
                      },
                      position:{
                          type: 'string',
                          default: ''
                      },
                      rotation:{
                          type: 'string',
                          default: ''
                      },
                      scale: {
                          type: 'string',
                          default: ''
                      },
                      'thumbnail-url': {
                          type: 'string'
                      },
                      category: {
                          type: 'string',
                          default: ''
                      },
                      active: {
                          type: 'boolean',
                          default: false
                      }
                  },
                  required: ["type"],
                  dependencies: {
                      'type': {
                          'oneOf': [
                              {
                                  'properties': {
                                      type: {
                                          enum: [
                                              'Embedded'
                                          ]
                                      },
                                      'content-url': {
                                          'type': 'string'
                                      }
                                  },
                                  
                              },
                              {
                                  'properties': {
                                      type: {
                                          enum: ['Navigation']
                                      },
                                      
                                      'content-url': {
                                          'type': 'string'
                                      }
                                  }
                              },
                              {
                                  'properties': {
                                      type: {
                                          enum: ['Image']
                                      }
                                  }
                              },
                              {
                                  'properties': {
                                      type: {
                                          enum: ['Video']
                                      }
                                  }
                              },
                              {
                                  'properties': {
                                      type: {
                                          enum: ['Pdf']
                                      }
                                  }
                              },
                              {
                                  'properties': {
                                      type: {
                                          enum: ['Custom']
                                      },
                                      'custom-type': {
                                          'type': 'string',
                                          require: true
                                      }
                                  }
                              }
                          ]
                      }
                  }
              }
            },
            media: {
                  type: 'array',
                  title: 'Media',
                  items: {
                    type: 'object',
                      title: 'Media',
                      properties: {
                          id: {
                              type: 'string',
                              default: ''
                          }, 
                          type: {
                              type: 'string',
                              default: '',
                              enum: ['Select Type', 'Navigation', 'Image', 'Video', 'Embedded', 'Pdf','Custom']
                          },
                          folder: {
                              type: 'string',
                              default: ''
                          },
                          category: {
                              type: 'string',
                              default: ''
                          },
                          'content-url': {
                              type: 'string'
                          }
                      },
                      dependencies: {
                          'type': {
                              'oneOf': [{
                                      'properties': {
                                          type: {
                                              enum: [
                                                  'Embedded'
                                              ]
                                          },
                                          'thumbnail-url': {
                                              'type': 'string'
                                          }
                                      },
                                  },
                                  {
                                      'properties': {
                                          type: {
                                              enum: ['Navigation']
                                          }
                                      }
                                  },
                                  {
                                      'properties': {
                                          type: {
                                              enum: ['Image']
                                          }
                                      }
                                  },
                                  {
                                      'properties': {
                                          type: {
                                              enum: ['Video']
                                          }
                                      }
                                  },
                                  {
                                      'properties': {
                                          type: {
                                              enum: ['Pdf']
                                          }
                                      }
                                  },
                                  {
                                      'properties': {
                                          type: {
                                              enum: ['Custom']
                                          },
                                          'custom-type': {
                                            'type': 'string',
                                            require: true
                                          }
                                      }
                                  }
                              ]
                          }
                      }
                  }
              }  
          }
        }
      }
    }
}
