// import React, { useEffect, useState } from 'react';
// import JSONSchemaForm from './Components/JSONSchemaForm/JSONSchemaForm';
// import ReactJson from "react-json-view";
// import GetAppIcon from '@material-ui/icons/GetApp';
// import CloudUploadIcon from '@material-ui/icons/CloudUpload';
// import exportFromJSON from 'export-from-json'
// import { Avatar, Backdrop, Button, CircularProgress, Dialog, DialogTitle, List, ListItem, ListItemAvatar, ListItemText } from '@material-ui/core';
// import { connect } from 'react-redux';
// import { setJsonData } from './Redux/actions/actions';
// import HiverlabLogo from './Assets/hiverlab-logo.png';
// import './App.css'
// import { Add, Save } from '@material-ui/icons';
// import Axios from 'axios';
// import { toast } from 'react-toastify';
// import DescriptionIcon from '@material-ui/icons/Description';
// import 'react-toastify/dist/ReactToastify.css';
// toast.configure();

// function App({setJSONFileData, JSONFileData}) {
//     const [res, setRes] = useState({});
//     const [openAllFilesDialog, setOpenAllFilesDialog] = useState(false)
//     const [allSavedFiles, setAllSavedFiles] = useState([]);
//     const [isLoading, setIsLoading] = useState(false);
//     useEffect(()=> {
//       if(Object.keys(JSONFileData).length>0){
//         setRes(JSONFileData)
//       }
//     }, [res, JSONFileData])

//     let fileReader;
//     const handleFileRead = (e) => {
//       const content = fileReader.result;
//       const parsedContent = JSON.parse(content);
//       setJSONFileData(parsedContent);
//     }
//     const fileChangeHandler = event => {
//       const file = event.target.files[0];
//       if (file.type !== "application/json") {
//         alert('Please upload a JSON file.')
//         return;
//       } else {
//         fileReader = new FileReader();
//         fileReader.onloadend = handleFileRead;
//         fileReader.readAsText(file);
//       }
//     }

//     const openFileAsJsonHandler = file => {
//       setIsLoading(true);
//       setOpenAllFilesDialog(false);
//       var url = 'https://cors-anywhere.herokuapp.com/' +file;
//       fetch(url)
//         .then(function (response) {
//           response.text().then(function (text) {
//             const jsonData = JSON.parse(text);
//             setJSONFileData(jsonData);
//             setIsLoading(false);
//           });
//         })
//         .catch(err=>{
//           console.log(err);
//           setIsLoading(false);
//           alert('Something went wrong...');
//           return
//         })
//     }

//     async function fetchAllSavedFilesHandler() {
//       setIsLoading(true);
//       let response;
//       try{
//         response = await Axios.get("https://hivemind.hiverlab.com/v1/api/json-editor/getExistingJSONs", {
//            headers: {
//              'SESSION-TOKEN': "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5MGFmODJmYS1hY2UwLTRlMzctYTgxNi03OTAzNDNjMjIyZDQiLCJqdGkiOiJmY2VmNmNjNzM2ZTczMzM4NWNiZmMyZTAwNzY0OTQ1NzY0ODNkYjc0ODBjM2ViZmFjOTA3YWU0MWY5Y2UyNWVmNTdkMjVmOTZjNjcxZGNmMSIsImlhdCI6MTYwMDkzMDM0NCwibmJmIjoxNjAwOTMwMzQ0LCJleHAiOjE2MzI0NjYzNDQsInN1YiI6IjQ3OSIsInNjb3BlcyI6W119.C9Upzu6gCyEfNc7M_2U77cRq05UOvLHSTcKNOf120YqvLXUOb8fNG0Gevy9ub34AvrqCVIbUIUOLPm9LKzBI0PiPwab04vWliO9S-oeMozPgZvsa3jIaJ1hqyWk8izOL5o57EbSLK00hEPGcaqxYjuIrovOJIU6H9ZPfR7EqLbEjcbym8IV2rZ8eX6neUyvTKop2EttlYFXtgaizFNSxgpSbtZ016pD0HiVpWZCHty9dq6IdfJ0CHpArjaPA_qPkYfukr9HjYiv5UoCqCnLsyHWyFhFSnrZQEqX9Vkpi_SadF4engT9uGx-_V-pS1gf26FQQtycPyF4gs93qu5hYPomwmchu2qwq6DKE1pOWz3WU5vY8ezr_OAEeKAKyKh7_3nDrZdozp7o8XI0qnv3hPRq396HJBpMk8wu33d7XYbVP5ugAyc8TSjSsh7uCxxtS7oJ2IKhK2BAb95ZqP-BfMPnlsC7mqGCY6OMTMes8fphAClXfktAXkQlANxIrZBxYsJfva2W_FFD-Mw0Wk6VPA6bgB5zFyWrdDh3gOcN5KqfznxmC2-l5v6KBMzH79DEwHetfM8y1c8crY8s0IbleJxUL-Mqgji1HX3v-vB3PO0LR9yBSgPTBVguLD6CfE80Hy4bnMp6hu5sj8Y7pP3Q-CrtMQX_upyihm-lx2GZ57Gc"
//            }
//         })
//       } catch(err){
//         console.log(err)
//         setIsLoading(false);
//         alert('Something went wrong!!!')
//       }
//       if(response){
//         console.log(response.data)
//         setIsLoading(false);
//         setAllSavedFiles(response.data.data);
//         setOpenAllFilesDialog(true);
//       }
//     }
//     async function saveJsonFileHandler(){
//       setIsLoading(true);
//       const jsonFile = new File([JSON.stringify(res, null, 4)], new Date().getTime().toString(),{
//         type: 'application/json'
//       });
//       const formData = new FormData();
//       formData.append('file', jsonFile)
//       formData.append('user_id', 479)
//       for(let pair of formData.entries()){
//         console.log(pair)
//       }
//       let response;
//       try {
//         response = await Axios.post(`https://hivemind.hiverlab.com/v1/api/json-editor/uploadEditorJSON`, formData, {
//           headers: {
//             'SESSION-TOKEN': "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5MGFmODJmYS1hY2UwLTRlMzctYTgxNi03OTAzNDNjMjIyZDQiLCJqdGkiOiJmY2VmNmNjNzM2ZTczMzM4NWNiZmMyZTAwNzY0OTQ1NzY0ODNkYjc0ODBjM2ViZmFjOTA3YWU0MWY5Y2UyNWVmNTdkMjVmOTZjNjcxZGNmMSIsImlhdCI6MTYwMDkzMDM0NCwibmJmIjoxNjAwOTMwMzQ0LCJleHAiOjE2MzI0NjYzNDQsInN1YiI6IjQ3OSIsInNjb3BlcyI6W119.C9Upzu6gCyEfNc7M_2U77cRq05UOvLHSTcKNOf120YqvLXUOb8fNG0Gevy9ub34AvrqCVIbUIUOLPm9LKzBI0PiPwab04vWliO9S-oeMozPgZvsa3jIaJ1hqyWk8izOL5o57EbSLK00hEPGcaqxYjuIrovOJIU6H9ZPfR7EqLbEjcbym8IV2rZ8eX6neUyvTKop2EttlYFXtgaizFNSxgpSbtZ016pD0HiVpWZCHty9dq6IdfJ0CHpArjaPA_qPkYfukr9HjYiv5UoCqCnLsyHWyFhFSnrZQEqX9Vkpi_SadF4engT9uGx-_V-pS1gf26FQQtycPyF4gs93qu5hYPomwmchu2qwq6DKE1pOWz3WU5vY8ezr_OAEeKAKyKh7_3nDrZdozp7o8XI0qnv3hPRq396HJBpMk8wu33d7XYbVP5ugAyc8TSjSsh7uCxxtS7oJ2IKhK2BAb95ZqP-BfMPnlsC7mqGCY6OMTMes8fphAClXfktAXkQlANxIrZBxYsJfva2W_FFD-Mw0Wk6VPA6bgB5zFyWrdDh3gOcN5KqfznxmC2-l5v6KBMzH79DEwHetfM8y1c8crY8s0IbleJxUL-Mqgji1HX3v-vB3PO0LR9yBSgPTBVguLD6CfE80Hy4bnMp6hu5sj8Y7pP3Q-CrtMQX_upyihm-lx2GZ57Gc"
//           }
//         })
//       } catch(err){
//         console.log(err);
//         setIsLoading(false)
//         alert('Oops..something went wrong!')
//         return;
//       }
//       if(response){
//         setIsLoading(false);
//         toast.success('JSON file saved successfully!', {
//           position: "bottom-left",
//           autoClose: 2000,
//           hideProgressBar: false,
//           closeOnClick: true,
//           pauseOnHover: true,
//           draggable: true
//         })
//       }
//     }

//     const downloadJsonFileHandler = () => {
//       const data = JSON.stringify(res, null, 4)
//       const fileName = new Date().getTime().toString();
//       const exportType = 'json'
//       exportFromJSON({ data, fileName, exportType })
//     }
    
//     return (
//       <React.Fragment>
//         <div className="top-bar" style={{background:"#1A1A1A", color:"white"}}>
//           <div className="row" style={{margin:"auto 1%"}}>
//           <div className="col-sm-4" style={{padding:"0 7px"}}>
//             <div className="hiverlab-logo" style={{padding:"10px 0"}}>
//               <img src={HiverlabLogo} alt="" style={{width:"105px", maxWidth:"100%", height:"auto", color:"#FDD344"}}/>
//             </div>
//           </div>
//           <div className="col-sm-4">
//           <h1 className="editor-title" style={{marginTop:"16px", fontSize:"1.7em"}}>CloudExpo JSON Editor</h1>
//           </div>
//           <div className="col-sm-4">
//             <div className="options-title" style={{marginTop:"11px"}}>
//                 <Button className="icon-button" component="span" onClick={fetchAllSavedFilesHandler}><DescriptionIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"4px", color:"#FDD344"}}>All Saved Files</span></Button>
//                 <input type="file" accept="application/json" onChange={(e)=>fileChangeHandler(e)} style={{display: "none"}} id="upload-file" />
//                 <label htmlFor="upload-file">
//                   {/* <Tooltip placement="bottom" arrow title="Import JSON File"> */}
//                     <Button className="icon-button" component="span" aria-label="Upload File"><CloudUploadIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"5px", color:"#FDD344"}}>Upload</span></Button>
//                   {/* </Tooltip> */}
//                 </label>
//               { Object.keys(res).length>0 ?
//                   <Button className="icon-button" component="span" onClick={downloadJsonFileHandler}><GetAppIcon style={{color:"#FDD344", fontSize:"1.5em"}} className="text-white"/><span style={{marginLeft:"1px", color:"#FDD344"}}>Download</span></Button>
//               : null }
//               { Object.keys(res).length>0 ?
//                   <Button className="icon-button" component="span" onClick={saveJsonFileHandler}><Save style={{color:"#FDD344", fontSize:"1.3em", marginTop:"-3px"}} className="text-white"/><span style={{marginLeft:"3px", color:"#FDD344"}}>Save</span></Button>
//               : null }
//             </div>
//             </div>
//           </div>
//         </div>

//         <div className="m-2 mt-3 p-4 bg-light">
//           <div className="row" style={{marginRight:"0px"}}>
//             <div className="col-lg-6 col-md-12 col-sm-12 mb-5 editor-form">
//               <h2 style={{color:"#FDD344", fontSize:"18px"}}>EDITOR</h2>
//               {/* <p>Below is the JSON editor generated from the JSON Schema.</p> */}
//               <JSONSchemaForm handleSubmit={payload => setRes(payload)} />
//             </div>
//             {/* <Divider orientation="vertical" style={{color:"white"}} /> */}
//             <div className="col-lg-6 col-md-12 col-sm-12 mb-5 json-structure">
//               <h2 style={{color:"#FDD344", fontSize:"18px"}}>JSON STRUCTURE</h2>
//               {/* <p>Below is the JSON representation generated from the HTML Form.</p> */}
//               <ReactJson src={res} />
//             </div>
//           </div>
//         </div>
//         <Dialog onClose={()=>setOpenAllFilesDialog(false)} aria-labelledby="simple-dialog-title" open={openAllFilesDialog}>
//           <DialogTitle id="simple-dialog-title" style={{background:"rgb(26,26,26)"}}><span style={{fontSize:"2.5rem", color:"white"}}>Saved Files</span></DialogTitle>
//           <List style={{background:"#0d0d0f"}}>
//           {
//             allSavedFiles.map(file => 
//             <ListItem autoFocus button onClick={()=>openFileAsJsonHandler(file.url)}>
//               <ListItemAvatar>                
//                   <Avatar data-toggle="tooltip" title="Open File">
//                     <Add style={{color:"black", fontWeight:500}} />
//                   </Avatar>
//               </ListItemAvatar>
//               <ListItemText>
//                 <span style={{color:"#fdd344", fontSize:"2rem"}}>{file.url.split("/")[file.url.split("/").length-1].split(".")[0]}</span>
//               </ListItemText>
//             </ListItem>
//           )}
//           </List>
//       </Dialog>
//       <Backdrop style={{color: "#fff", zIndex: 1201}} open={isLoading}>
//         <CircularProgress color="inherit" />
//       </Backdrop>
//       </React.Fragment>

//     );
// }

// const mapStateToProps = state => {
//   return {
//     JSONFileData: state.json.jsonStructuredData
//   }
// }

// const mapDispatchToProps = dispatchEvent => {
//   return {
//     setJSONFileData: (data) => dispatchEvent(setJsonData(data))
//   }
// }
// export default connect(mapStateToProps,mapDispatchToProps)(App);
import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Login from './Components/Auth/Login/Login'
import Dashboard from './Components/Dashboard/Dashboard'
import {ProtectiveRoute} from './Components/Auth/ProtectiveRoute';
import './App.css'
function App() {
  return (
    <BrowserRouter>
      <ProtectiveRoute exact path="/" component={Dashboard}></ProtectiveRoute>
      <Route exact path="/login" component={Login}></Route>
    </BrowserRouter>
  )
}

export default App
